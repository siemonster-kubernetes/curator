#!/bin/sh

if [ -n "$CONFIG_FILE" ]; then
    echo "Copying a file /etc/config/config.yml from CONFIG_FILE environment variable"
    mkdir -p /etc/config
    echo "$CONFIG_FILE" > /etc/config/config.yml
fi

if [ -n "$ACTION_FILE" ]; then
    echo "Copying a file /etc/config/action_file.yml from ACTION_FILE environment variable"
    mkdir -p /etc/config
    echo "$ACTION_FILE" > /etc/config/action_file.yml
fi

gosu nobody:nobody "/curator/curator" "$@"
